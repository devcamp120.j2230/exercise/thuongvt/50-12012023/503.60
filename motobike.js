import { Vehice } from "./vehice.js";
// Khai báo một lớp là Motobike kế thừ thuộc tính Class Phương tiện giao thông và có những thuộc tính mà lớp phương tiện giao thông không có.
class Motobike extends Vehice {
    // Khai báo thuộc tính mà class Vehice không có
    vId;
    modelName;
    // khai báo phương thức khởi tạo cho lớp Motobike
constructor (paramBrand, paramYearManufacured,paramVId, paramModelName){
    // sử dụng phương thức super gán giá trị cho Brand và yearManufactured
    super(paramBrand, paramYearManufacured)
    this.vId = paramVId;
    this.modelName = paramModelName;
    };
//Khai báo phương thức cho lớp Car
honk() {
    return "Kính cong"
    }
}

export {Motobike}