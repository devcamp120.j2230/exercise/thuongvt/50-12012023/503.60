import { Vehice } from "./vehice.js";
// khai báo một lớp là car (xe oto) kế thừa thuộc tính của lớp phương tiện giao thông và có những thuộc tính mà lớp chung phương tiện giao thông không có
class Car extends Vehice {
    // khai báo thuộc tính mà class Vehice không có
    vId;
    modelName;
    // khai báo phương thức khởi tạo cho lớp Car
    constructor (paramBrand, paramYearManufacured,paramVId, paramModelName){
        // sử dụng phương thức super gán giá trị cho Brand và yearManufactured
        super(paramBrand, paramYearManufacured)
        this.vId = paramVId;
        this.modelName = paramModelName;
    };
    //Khai báo phương thức cho lớp Car
    honk() {
        return "Bíp bíp"
        }
    }

    export { Car }