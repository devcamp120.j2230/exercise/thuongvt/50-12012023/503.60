//khái báo một lớp là phương tiện giao thông chung dùng cho cả một lớp
class Vehice {
    //khai báo các thuộc tính của class
    brand;
    yearManufactured;
    // Khai báo hàm khởi tạo cho class
    constructor (paramBrand, paramYearManufacured) {
        //gán giá trị cho thuộc tính
        this.brand = paramBrand;
        this.yearManufactured = paramYearManufacured;
    };
    //Khai báo phương thức cho class
    print() {
       return "cái này là phương tiện giao thông nha"
    }
}

export {Vehice}