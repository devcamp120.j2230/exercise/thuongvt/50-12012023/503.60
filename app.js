 
    import {Vehice} from "./vehice.js";
    import { Car } from "./car.js";
    import { Motobike } from "./motobike.js";
    
var toyota = new Vehice ("h500", 2023);
console.log(toyota.brand);
console.log(toyota.yearManufactured);
console.log(toyota.print());

var Mes = new Car ("h500", 2023, 123, "abc");
console.log(Mes.brand);
console.log(Mes.yearManufactured);
console.log(Mes.vId);
console.log(Mes.modelName);
console.log(Mes.print());
console.log(Mes.honk())

var Dream = new Motobike ("h500", 2023, 789, "xyz");
console.log(Dream.brand);
console.log(Dream.yearManufactured);
console.log(Dream.vId);
console.log(Dream.modelName);
console.log(Dream.print());
console.log(Dream.honk())